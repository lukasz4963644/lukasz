<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain;

use App;
use App\Domain\Delegation\DelegationCostCalculator;
use App\Domain\Delegation\DelegationCostRule\CountryDelegationCostRule;
use App\Domain\Delegation\DelegationCostRule\LengthDelegationCostRule;
use App\Domain\Delegation\DelegationCostRule\MinimumHaoursInDayDelegationCostRule;
use App\Domain\Delegation\DelegationCostRule\WorkDayDelegationCostRule;
use DateTime;
use PHPUnit\Framework\TestCase;

class DelegationCostCalculatorTest extends TestCase
{
    /**
     * @dataProvider dateRangeProvider
     */
    public function testCalculateHoursPerDay(
        string $startDatetime,
        string $endDatetime,
        string $country,
        int $expectedResult
    ) {
        $calculator = new DelegationCostCalculator(
            new \App\Domain\Delegation\DelegationHoursPerDayCalculator(),
            new CountryDelegationCostRule(),
            new LengthDelegationCostRule(),
            new MinimumHaoursInDayDelegationCostRule(),
            new WorkDayDelegationCostRule()
        );
        $delegation = new App\Domain\Delegation\Delegation(
            new \App\Domain\Employee\Employee(1),
            new \App\Domain\Country($country),
            new DateTime($startDatetime),
            new DateTime($endDatetime)
        );

        $result = $calculator->calculateDelegationCost($delegation);

        $this->assertEquals($expectedResult, $result);
    }

    public static function dateRangeProvider(): array
    {
        return [
            ['2023-11-01 08:00:00', '2023-11-03 11:00:00', 'PL', 30],
            ['2023-11-01 08:00:00', '2023-11-01 16:00:00', 'PL', 10],
            ['2023-11-01 08:00:00', '2023-11-01 11:00:00', 'PL', 0],
            ['2023-11-03 08:00:00', '2023-11-01 11:00:00', 'PL', 0],
            ['2023-11-01 08:00:00', '2023-11-05 11:00:00', 'PL', 30], //sb, nd
            ['2023-11-01 08:00:00', '2023-11-08 11:00:00', 'PL', 70], //sb, nd, 7 dni
            ['2023-11-01 08:00:00', '2023-11-08 11:00:00', 'DE', 350], //sb, nd, 7 dni
            ['2023-11-01 08:00:00', '2023-11-08 11:00:00', 'GB', 525], //sb, nd, 7 dni
        ];
    }
}


