<?php

namespace App\Domain;

class Currency
{
    public const PLN = 'PLN';

    private string $code;

    public function __construct()
    {
        $this->code = self::PLN;
    }

    public function getCode(): string
    {
        return $this->code;
    }

}

