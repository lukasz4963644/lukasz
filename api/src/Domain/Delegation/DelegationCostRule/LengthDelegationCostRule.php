<?php

declare(strict_types=1);

namespace App\Domain\Delegation\DelegationCostRule;

class LengthDelegationCostRule
{
    public function calculateMultiplier(int $days): int
    {
       if ($days > 7) {
           return 2;
       } else {
           return 1;
       }
    }
}
