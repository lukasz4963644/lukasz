<?php

declare(strict_types=1);

namespace App\Domain\Delegation\ValidationRule;

use App\Domain\Delegation\Delegation;
use App\Domain\Delegation\DelegationRepository;
use App\Domain\Employee\Employee;

class OneDelegationOnTimeRule
{
    private DelegationRepository $delegationRepository;

    public function __construct (
        DelegationRepository $delegationRepository
    ) {
        $this->delegationRepository = $delegationRepository;
    }

    public function check(
        Delegation $delegation,
        Employee $employee
    ): bool {
        return !$this->delegationRepository->exist(
            $delegation,
            $employee
        );
    }
}
