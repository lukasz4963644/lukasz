<?php

declare(strict_types=1);

namespace App\Domain\Delegation;

use App\Domain\Country;
use App\Domain\Employee\Employee;
use DateTimeInterface;

class Delegation
{
    private Country $country;
    private Employee $employee;
    private DateTimeInterface $startDate;
    private DateTimeInterface $endDate;

    public function __construct(
        Employee $employee,
        Country $country,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    )
    {
        $this->employee = $employee;
        $this->country = $country;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    public function getStartDate(): DateTimeInterface
    {
        return $this->startDate;
    }

    public function getEndDate(): DateTimeInterface
    {
        return $this->endDate;
    }

}
