<?php

declare(strict_types=1);

namespace App\Domain\Delegation;

use App\Application\Entity\Employee;

interface DelegationRepository
{
    public function create(
        \App\Domain\Delegation\Delegation $delegation,
        \App\Domain\Employee\Employee $employee
    ): Delegation;


    public function exist(
        \App\Domain\Delegation\Delegation $delegation,
        \App\Domain\Employee\Employee $employee
    ): bool;
}
