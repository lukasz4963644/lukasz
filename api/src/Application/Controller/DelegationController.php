<?php

declare(strict_types=1);

namespace App\Application\Controller;

use App\Application\DelegationValidator;
use App\Domain\Currency;
use App\Domain\Delegation\DelegationCostCalculator;
use App\Application\DelegationManager;
use App\Application\Dto\Delegation;
use App\Application\Dto\DelegationWithCost;
use App\Application\Dto\Employee;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Method contains operations related to delegation
 *
 * @OA\Tag(name="Delegation")
 */
class DelegationController extends AbstractController
{
    /**
     * @OA\Post(
     *     summary="Add new delegation",
     *     @OA\RequestBody(
     *         description="Delegation data",
     *         @OA\JsonContent(
     *             ref=@Model(type=Delegation::class)
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Ok",
     *     @OA\JsonContent(
     *        ref=@Model(type=Delegation::class)
     *     )
     * )
     *
     * @OA\Response(
     *     response=400,
     *     description="Validation error",
     *     @OA\JsonContent(
     *         @OA\Property(
     *             property="error",
     *             type="string",
     *             example="The start date of the delegation cannot be later than the end date of the delegation, and at the same time, an employee can be present on only one delegation"
     *         )
     *     )
     * )
     *
     */
    #[Route('api/delegation', name: 'delegation_add', methods: ['POST'])]
    public function add(
        Request $request,
        SerializerInterface $serializer,
        DelegationManager $delegationManager,
        DelegationValidator $delegationValidator
    ): JsonResponse {

        $delegation = $serializer->deserialize(
            $request->getContent(),
            Delegation::class,
            'json'
        );

        if (!$delegationValidator->isValid($delegation)) {
            return new JsonResponse(
                $serializer->serialize('The start date of the delegation cannot be later than the end date of the delegation, and at the same time, an employee can be present on only one delegation', 'json'),
                Response::HTTP_BAD_REQUEST,
                [],
                true
            );
        }

        $delegationManager->createDelegation($delegation);

        return new JsonResponse(
            $serializer->serialize(null, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @OA\Get(
     *     summary="Get all delegations with cost for a specific employee",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the employee",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Ok",
     *     @OA\JsonContent(
     *        ref=@Model(type=DelegationWithCost::class)
     *     )
     * )
     *
     */
    #[Route('api/delegation/{id}', name: 'delegation_get', methods: ['GET'])]
    public function get(
        SerializerInterface $serializer,
        DelegationManager $delegationManager,
        DelegationCostCalculator $delegationCostCalculator,
        int $id
    ): JsonResponse {

        $employee = new Employee($id);
        $delegations = $delegationManager->readDelegation($employee);

        $delegationsWithCost = [];

        foreach ($delegations as $delegation)
        {
            $delegationsWithCost[] = new DelegationWithCost(
                $delegation->getStartDate(),
                $delegation->getEndDate(),
                $delegation->getCountry()->getCode(),
                $delegationCostCalculator->calculateDelegationCost($delegation),
                (new Currency())->getCode()
            );
        }

        return new JsonResponse(
            $serializer->serialize($delegationsWithCost, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

}
