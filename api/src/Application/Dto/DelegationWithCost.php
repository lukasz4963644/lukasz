<?php

declare(strict_types=1);

namespace App\Application\Dto;

use DateTimeInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     description="DelegationWithCost model",
 *     title="DelegationWithCost",
 *     required={"start", "end", "country", "amountDue", "currency"}
 * )
 */
class DelegationWithCost
{
    /**
     * @OA\Property(
     *     type="string",
     *     format="date-time",
     *     description="Start date and time",
     *     example="2023-11-01 08:00:00"
     * )
     */
    public string $start;

    /**
     * @OA\Property(
     *     type="string",
     *     format="date-time",
     *     description="End date and time",
     *     example="2023-11-03 16:00:00"
     * )
     */
    public string $end;


    /**
     * @OA\Property(
     *     type="string",
     *     description="Country code ISO 3166-1 alpha-2",
     *     example="PL"
     * )
     */
    public string $country;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Aamount due",
     *     example="150"
     * )
     */
    public int $amountDue;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Currency",
     *     example="PLN"
     * )
     */
    public string $currency;

    public function __construct(
        DateTimeInterface $start,
        DateTimeInterface $end,
        string $country,
        int $amountDue,
        string $currency
    ) {
        $this->start = $start->format('Y-m-d H:i:s');
        $this->end = $end->format('Y-m-d H:i:s');
        $this->country = $country;
        $this->amountDue = $amountDue;
        $this->currency = $currency;
    }
}
