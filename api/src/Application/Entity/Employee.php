<?php

declare(strict_types=1);

namespace App\Application\Entity;

use App\Application\Repository\EmployeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'employee', schema: 'public')]
#[ORM\Entity(repositoryClass: EmployeeRepository::class)]
class Employee
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'employee_id', type: 'integer')]
    private int $id;

    #[ORM\OneToMany(mappedBy: 'employee', targetEntity: Delegation::class, cascade: ['persist', 'remove'])]
    private Collection $delegations;

    public function __construct()
    {
        $this->delegations = new ArrayCollection();
    }

    /**
     * @return Delegation[]
     */
    public function getDelegations(): Collection
    {
        return $this->delegations;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

}


